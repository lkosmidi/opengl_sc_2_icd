CC=gcc
LIBRARY_CFLAGS=-fpic -shared -I.
CFLAGS=-Wall -pedantic -g -O0
LDFLAGS=-lGLESv2 -lEGL -lX11

all: libGLSC2.so glsc2_offline_compiler 

libGLSC2.so: glsc2.c
	$(CC) -o $@ $(LIBRARY_CFLAGS) glsc2.c

glsc2_offline_compiler: glsc2_offline_compiler.o esUtil.o
	$(CC) -o $@ $(CFLAGS) glsc2_offline_compiler.o esUtil.o $(LDFLAGS)

glsc2_offline_compiler.o: glsc2_offline_compiler.c
	$(CC) $(CFLAGS) -c glsc2_offline_compiler.c

esUtil.o: esUtil.h esUtil.c
	$(CC) -c $(CFLAGS) esUtil.c

clean:
	rm -f glsc2_offline_compiler *.so *.o
