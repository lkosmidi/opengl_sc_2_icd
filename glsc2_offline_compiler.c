#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <EGL/egl.h>
#include "esUtil.h"

void print_usage(char* program_filename)
{
	printf("Usage\n:");
	printf("%s vertex_shader_filename fragment_shader_filename program_binary_filename\n", program_filename);
}

void parse_arguments(int argc, char** argv, char** vertex_shader_filename, char** fragment_shader_filename, char** program_filename)
{
	if(argc < 4)
	{
		print_usage(argv[0]);
		exit(-1);
	}

	*vertex_shader_filename=argv[1];
	*fragment_shader_filename=argv[2];
	*program_filename=argv[3];
}

char* read_file(char* filename)
{
	FILE* file=fopen(filename, "r");
	long filelen=0;
	char * source_code=NULL;
	
	if(!file)
	{
		printf("file %s does not exist\n", filename);
		exit(-1);
	}
	fseek(file, 0, SEEK_END);
	filelen=ftell(file);
	printf("file length=%ld\n", filelen);
	rewind(file);
	source_code = malloc((filelen*sizeof(char))+1);
	fread(source_code, filelen, sizeof(char), file);
	source_code[filelen]='\0';

	printf("Source code of file %s\n%s\n", filename, source_code);

	return source_code;
}

ESContext esContext;

void InitContexts(void)
{
	esInitContext ( &esContext );
	esCreateWindow ( &esContext, "OpenGL SC 2.0 Offline Compiler", 1, 1, ES_WINDOW_RGB|ES_WINDOW_ALPHA);
}


GLuint createShader(const char* shader_source_code, char* filename, GLenum shaderType)
{
	GLuint id = 0;
	GLint status = 0;
	GLint shaderlen = strlen(shader_source_code);

	id = glCreateShader(shaderType);
	if(id==0)
	{
		printf("cannot create shader\n");
		exit(-1);
	}
	glShaderSource(id, 1, &shader_source_code, &shaderlen);
	glCompileShader(id);
	glGetShaderiv(id, GL_COMPILE_STATUS, &status);

	if(status!=GL_TRUE)
	{
		char *errlog;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &status);
		errlog = (char *) malloc(status*sizeof(char));
		glGetShaderInfoLog(id, status, NULL, errlog);
		printf( "Shader %s Compilation error:\n%s\n", filename, errlog);
		fflush(stderr);
		free(errlog);
		exit(1);
	}
	return id;
}

GLuint createProgram(GLuint vShader, GLuint fShader)
{
	GLint status=0;
	GLuint program_id = glCreateProgram();
	if(program_id==0)
	{
		printf("Cannot create program\n");
		exit(-1);
	}
	glAttachShader(program_id, vShader);
	glAttachShader(program_id, fShader);
	glLinkProgram(program_id);
	glGetProgramiv(program_id, GL_LINK_STATUS, &status);

	if(status!=GL_TRUE)
	{
		printf("Cannot Link shaders\n");
		exit(-1);
	}

	return program_id;
}

void write_program(GLuint program_id, char* filename)
{
	FILE* file=NULL;
	GLint binary_length=0;
	GLenum binary_format;
	void * binary=NULL;

	//get the pointer to the glGetProgramBinaryOES function
	PFNGLGETPROGRAMBINARYOESPROC glGetProgramBinaryOES = (PFNGLGETPROGRAMBINARYOESPROC)eglGetProcAddress("glGetProgramBinaryOES");
	if (glGetProgramBinaryOES == NULL)
	{
		printf("This system doesn't support saving the GPU program binary\n");
	}

	//Get the binary from the program object
	glGetProgramiv(program_id, GL_PROGRAM_BINARY_LENGTH_OES, &binary_length);
	printf("Program length: %d\n", binary_length);
	binary = (void*)malloc(binary_length);
	glGetProgramBinaryOES(program_id, binary_length, NULL, &binary_format, binary);

	file = fopen(filename, "wb");
	//write first the size
	fwrite(&binary_length, 1, sizeof(GLint), file);
	//now the binary_format
	fwrite(&binary_format, 1, sizeof(GLenum), file);
	//Write the program binary
	fwrite(binary, 1, binary_length, file);
	fclose(file);
	free(binary);
}

int main(int argc, char ** argv)
{
	char* vertex_shader_filename=NULL;
	char* vertex_shader_source_code=NULL;
	char* fragment_shader_filename=NULL;
	char* fragment_shader_source_code=NULL;
	char* program_filename=NULL;
	GLuint vShader=0, fShader=0;
	GLuint program_id=0;

	parse_arguments(argc, argv, &vertex_shader_filename, &fragment_shader_filename, &program_filename);

	printf("vertex shader filename: %s\n", vertex_shader_filename);
	printf("fragment shader filename: %s\n", fragment_shader_filename);
	printf("program shader filename: %s\n", program_filename);

        InitContexts();

	vertex_shader_source_code=read_file(vertex_shader_filename);
	vShader = createShader(vertex_shader_source_code, vertex_shader_filename, GL_VERTEX_SHADER);

	fragment_shader_source_code=read_file(fragment_shader_filename);
	fShader = createShader(fragment_shader_source_code, fragment_shader_filename, GL_FRAGMENT_SHADER);

	program_id = createProgram(vShader, fShader);
	write_program(program_id, program_filename);

	return 0;
}
