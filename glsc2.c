#include <GLSC2/glsc2.h>
#include <GLES2/gl2.h>

#define MAX(x,y) (((x)>=(y))?(x):(y))

GL_APICALL void GL_APIENTRY glReadnPixels (GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data)
{
   /* simply ignore bufSize */
   glReadPixels (x, y, width, height, format, type, data);
}

GL_APICALL void GL_APIENTRY glTexStorage2D (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height)
{
   int i=0;
   for (i = 0; i < levels; i++)
   {
       /* glTexStorage2D behaves as glTexImage2D when pixels=NULL but it cannot be modified */
       /* TODO: decide format and type based on internalformat using Table 3.4*/
       glTexImage2D (target, i, internalformat, width, height, 0, internalformat, GL_UNSIGNED_BYTE, 0);
       width = MAX(1, (width / 2));
       height = MAX(1, (height / 2));
   }
}
